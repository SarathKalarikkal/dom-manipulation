const removeBtn =  document.querySelector('.remove')
const addBtn =  document.querySelector('.add')
const textContent =  document.querySelector('.text')

let textBackup = ''

removeBtn.addEventListener('click', ()=>{
    if(textContent){
        textBackup = textContent.innerHTML
        textContent.innerHTML = ''
    }
})
addBtn.addEventListener('click', ()=>{
    textContent.innerHTML = textBackup
})